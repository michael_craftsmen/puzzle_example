# puzzle_template

Contains branches:
- master: contains some puzzle examples
- puzzle_solution: will contain your puzzle *fully*
- puzzle_template: will contain your puzzle *basics*


**Part.1 - ADD YOUR PUZZLE**
1. Fork this project:
    
    https://gitlab.com/craftsmen/puzzle_example
    
    visiblity: public

    contains branches: master, puzzle_solution, puzzle_template


2. Results in:

    https://gitlab.com/your_username/puzzle_example
    
    visiblity: **private!**
    
    contains branches: master, puzzle_solution, puzzle_template

3. Add your teammates:

    see: https://gitlab.com/your_username/puzzle_template/project_members
    
    "Select members to invite" = name of your teammate
    
    "Choose a role permission" = maintainer
    
    Click button: "Add to project".

4. Create your puzzle with tests in branch 'puzzle_solution':
    
    on push, your puzzle is loaded into the game.

5. Minify your puzzle to a problem and basic tests only in branch 'puzzle_template'.

6. When done, press de 'publish puzzle' button of the 'puzzle_example' pipeline.